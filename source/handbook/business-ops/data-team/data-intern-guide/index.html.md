The following outline is a brief overview of the 16-week plan for a data team intern who is working part time.

The Data Intern Role is described on the [Data Analyst](/job-families/finance/data-analyst/) page.

Some of the benefits:
* This is a great way of practicing onboarding.
* We need to focus on not *just* educating data team members on the tools we're using (what they are) but also on how we're using them. Interns have less opinions and less experience making the education process magnified in importance.
* Working closely with and managing an intern is a great learning opportunity for members of the data team.
* Twelve to sixteen weeks is about the length of a college semester, so this outline could be applied to summer or academic year interns.

## Expected Timeline
Internships should be no less than 12 weeks, but an ideal length of time is 16 weeks.
Interns are expected to work no less than 50% time.
A part time intern will work between 30 (12*2.5) and 40 (16*2.5) work days.
A full time intern will work between 60 and 80 work days.

### Part Time Intern
Days 1-7 - GitLab onboarding & Data Team onboarding, emphasis on dbt
Day 8 - The importance of documentation to democratizing analyses
Days 9-10 - Inventory of existing documentation; Goal: List of dbt models that exist without any documentation, broken down by schema
Day 11 - The importance of defining work processes, e.g. dbt test coverage
Days 12-13 - Identify gaps in processes documentations; Goal: Make an MR to the data team handbook with info on where our processes are not properly documented
Days 14-16 - Analysis Shadow from start to finish with data analyst; start working with manager to identify independent work project; create MR to the data analysis process with something learned; Work with manager to finalize details of independent work project
Day 17 - The importance of testing to ensure data quality; Independent work project
Day 18-19 - Inventory of existing tests; Goal: List of dbt models that don't have (at least) not null and unique tests; Independent work project
Day 20-22 - Analysis Shadow from start to finish with data analyst; Independent work project
Day 23-27 - Work on an analysis while being closely monitored; Independent work project
Day 28 - Deliver independent work project to team in 40 minute presentation with 20 minute Q&A and in a blog post ([example](https://m.signalvnoise.com/twelve-weeks---basecamp--a-summer-tale/#.sirlipd52))
Day 29-30 - Offboard, revoke accesses

## Tasks
The tasks associated with each of these weeks should be stored in an issue template, so that they can be reused and quickly iterated on.
This blue print is simply an overview.
