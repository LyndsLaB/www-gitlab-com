/* global setupCountdown */

function setupHackathonCountdown() {
  var nextHackathonDate = new Date('May 29, 2019 00:00:00').getTime();

  setupCountdown(nextHackathonDate, 'nextHackathonCountdown');
}

(function() {
  setupHackathonCountdown();
})();

